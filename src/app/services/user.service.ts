import { Injectable } from '@angular/core';
import { map, switchMap } from 'rxjs/operators';
import { HttpClientService } from './httpclient.service';
import { AppSettingsService } from './appsettings.service';
import { getCurrent } from './utils';

@Injectable({
    providedIn: 'root'
})
export class UserService {
    public webServiceUrl$ = this.appsettingsService.appSettings$.pipe(map(it => it.WebServiceUrl));

    constructor(
      private httpClientService: HttpClientService,
      private appsettingsService: AppSettingsService
    ) { }

    private getRequest = <T>(url: string) => getCurrent(this.webServiceUrl$.pipe(
        switchMap(baseUrl => this.httpClientService.get(baseUrl + url)),
        map(x => x as T))
      )

    private postRequest = <T>(url: string, payload) => getCurrent(this.webServiceUrl$.pipe(
        switchMap(baseUrl => this.httpClientService.post(baseUrl + url, payload)),
        map(x => x as T)
    ))
}
