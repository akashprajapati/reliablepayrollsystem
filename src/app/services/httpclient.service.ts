// angular
import { Injectable } from '@angular/core';

// services
import { CookieService } from './cookie.service';
import { tap, catchError, map } from 'rxjs/operators';
import { Observable } from 'rxjs';
import { HttpClient, HttpHeaders, HttpRequest, HttpEventType } from '@angular/common/http';
import { AppSettingsService } from './appsettings.service';
import { throwError } from 'rxjs';
import { filterNull } from './utils';
import { ToastService } from './toast.service';
import { Constants } from '../models/constants';

@Injectable({
    providedIn: 'root'
})
export class HttpClientService {

    public requestIsActive = false;
    private baseUrl = this.appsettingsService.getApiUrl();
    constructor(private httpClient: HttpClient,
                private cookieService: CookieService,
                private appsettingsService: AppSettingsService,
                private toastService: ToastService) {
    }

    public setRequestIsActive(val: boolean) {
        this.requestIsActive = val;
    }

    private getHttpHeaders() {
        const options = {
            headers: new HttpHeaders()
                .set('Content-Type', 'application/json')
                .append('Authorization', 'Bearer ' + this.getJwtToken())
        };
        return options;
    }

    public post(url: string, body: any = {}) {
        return this.httpClient.post(url, body, this.getHttpHeaders()).pipe(
            tap(res => res),
            catchError((error: any) => this.onErrorHandler(error))
        );
    }

    public get(url: string) {
        return this.httpClient.get(url, this.getHttpHeaders()).pipe(
            tap(res => res),
            catchError((error: any) => this.onErrorHandler(error))
        );
    }

    public put(url: string, body: any) {
        return this.httpClient.put(url, body, this.getHttpHeaders()).pipe(
            tap(res => res),
            catchError((error: any) => this.onErrorHandler(error))
        );
    }

    public delete(url: string) {
        return this.httpClient.delete(url, this.getHttpHeaders()).pipe(
            tap(res => res),
            catchError((error: any) => this.onErrorHandler(error))
        );
    }

    private onErrorHandler(error: any) {
        if (error.error instanceof ProgressEvent) {
            this.toastService.addToast('', error.statusText, 'error');
        } else {
            if (error.error === 'Invalid Token') {
                this.cookieService.deleteCookie(Constants.RefToken);
                this.cookieService.deleteCookie(Constants.Token);
                document.location.href = '';
                return;
            }
            this.toastService.addToast('', error.error, 'error');
        }
        return throwError(error);
    }

    private request<T>(req: HttpRequest<T>): Observable<any> {
        return this.httpClient.request(req).pipe(
            tap(res => res),
            map(res => {
                console.log(res);
                return res.type === HttpEventType.Response ? res.body as any : undefined;
            }),
            filterNull(),
            catchError((error: any) => this.onErrorHandler(error)));
    }

    getJwtToken = () => this.cookieService.getCookie(Constants.Token);
}
