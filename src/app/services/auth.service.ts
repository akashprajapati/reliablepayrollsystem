import { Injectable } from '@angular/core';
import { Subject, defer } from 'rxjs';
import { CookieService } from './cookie.service';
import { JwtService } from './jwt.service';
import { AuthorizationService } from './authorization.service';
import { startWith, map, switchMap, repeatWhen, delay, distinctUntilChanged } from 'rxjs/operators';
import { filterNull, weakShareReplay, getCurrent } from './utils';
import { Constants } from '../models/constants';
import { UserService } from './user.service';

const timeDiff = (from: Date, until: Date): number => (Math.round((from.getTime() - until.getTime())));

@Injectable({
    providedIn: 'root'
})
export class AuthenticationService {

    private onNewJwtToken$ = new Subject();

    constructor(private cookieService: CookieService,
                private jwtService: JwtService,
                private authorizationService: AuthorizationService,
                private userService: UserService) {
                    const role = this.getRolesFromJwtToken();
                    this.isLoggedIn = role.length > 0;
                }

    public jwtTokenString$ = this.onNewJwtToken$.pipe(
        startWith({}),
        map(_ => this.getJwtToken()),
        filterNull(), // Only try to refresh JWT token when there actually is one
        switchMap(token => defer(() => this.maybeRefreshToken(token)).pipe(
            map(_ => this.getJwtToken()),
            repeatWhen(e => e.pipe(delay(5000))))
        ),
        distinctUntilChanged(),
        weakShareReplay(1));

    public isLoggedIn$ = this.jwtTokenString$.pipe(map(it => !!it));

    public isLoggedIn = false;
    async isAuthenticated() {
        return await getCurrent(this.isLoggedIn$);
    }

    async maybeRefreshToken(currentToken: string) {
        currentToken = this.getJwtToken();
        try {
            const { exp, tid } = this.jwtService.decodeJwtToken(currentToken);
            if (timeDiff(new Date(exp * 1000), new Date(Date.now())) > 10000) {
                return;
            }
        } catch (e) {
            await this.logout('ReasonTokenRefreshFailed');
            throw e;
        }
    }

    public login = () => this.onNewJwtToken$.next();

    logout(reason: string) {
        //this.userService.logout();
        this.cookieService.deleteCookie(Constants.RefToken);
        this.cookieService.deleteCookie(Constants.Token);
        this.isLoggedIn = false;
        //this.userService.user = null;
        this.onNewJwtToken$.next();
        this.redirectToLoginPage();
    }

    async LogonPortalSetCredentials(jwttoken: string, refreshtoken: string) {
        const parsedToken = this.jwtService.decodeToken(jwttoken);
        this.saveJwtToken(jwttoken);
        this.saveRefreshToken(refreshtoken, jwttoken);
        if (parsedToken.sub) {
            //this.userService.user = await this.userService.getUser(parsedToken.sub);
            this.isLoggedIn = true;
        }
        this.setPermission();
        this.onNewJwtToken$.next();
    }

    public getRolesFromJwtToken() {
        const jwtToken = this.cookieService.getCookie(Constants.Token);
        if (jwtToken) {
        const parsedToken = this.jwtService.decodeJwtToken(jwtToken);
        const role = parsedToken.role.toString().toLowerCase();
        const roles = role ? role.split(',') : [];
        return roles;
        }
        return [];
    }

    public async redirectToLoginPage() {
        document.location.href = '';
    }

    async setUser() {
        const jwt = this.getJwtToken();
        //this.userService.user = await this.userService.getUser(this.getLoggedInUser(jwt));
    }

    setPermission = () => this.authorizationService.setPermission(this.getRolesFromJwtToken());

    getLoggedInUser = (token: string) => this.jwtService.decodeToken(token).sub;

    getJwtToken = () => this.cookieService.getCookie(Constants.Token);
    getRefreshToken = () => this.cookieService.getCookie(Constants.RefToken);

    saveJwtToken(token: string) {
        document.cookie = Constants.Token + '=' + token + ';expires=' +
            new Date(this.jwtService.decodeJwtToken(token).exp * 1000 + 60000).toUTCString() + ';path=/';
    }

    saveRefreshToken = (token: string, jwtToken: string) => document.cookie = Constants.RefToken + '=' + token + ';expires=' +
        new Date(this.jwtService.decodeJwtToken(jwtToken).exp * 1000 + 60000).toUTCString() + ';path=/'
}
