// angular packages
import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';
import { map } from 'rxjs/operators';
import { getCurrent } from './utils';
import { CookieService } from './cookie.service';
import { JwtService } from './jwt.service';
import { Constants } from '../models/constants';

@Injectable({
    providedIn: 'root'
})
export class AuthorizationService {
    private apiPermissions$: Observable<string[]>;

    constructor(
        private cookieService: CookieService,
        private jwtService: JwtService) {
        this.apiPermissions$ = of(this.getRolesFromJwtToken());
    }

    setPermission(roles: string[]) {
        this.apiPermissions$ = of(roles);
    }

    // Return true if at least one of the requiredPermission is in the permissions list
    // acquired from the permission service
    public hasPermission$(requiredPermission: string[]): Observable<boolean> {
        return this.apiPermissions$.pipe(map(it =>
            !!requiredPermission
                .map(rp => rp.toLowerCase())
                .find(rp => !!it.find(p => p === rp))));
    }

    public async isAuthorisedAccess(role: string[]) {
        const isvalid = await getCurrent(this.hasPermission$(role));
        if (!isvalid) {
            document.location.href = '';
        }
    }

    private getRolesFromJwtToken() {
        const jwtToken = this.cookieService.getCookie(Constants.Token);
        if (jwtToken) {
        const parsedToken = this.jwtService.decodeJwtToken(jwtToken);
        const role = parsedToken.role.toString().toLowerCase();
        const roles = role ? role.split(',') : [];
        return roles;
        }
        return [];
    }
}
