export class Constants {
    public static Token = 'reliablepayrolltoken';
    public static RefToken = 'reliablepayrollreftoken';
}